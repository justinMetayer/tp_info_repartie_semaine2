package main

import "fmt"

type node struct {
	id string
	couleur int //0 blanc 1 grey 2 noir
}

type graph struct {
	fils []graph
	noeud node
}

func explorer(g []graph, nodeM node){
	fmt.Println("Coloration node : ", nodeM.id, " couleur : gris")
	nodeM.couleur = 1
	for _, element := range g{
		if(element.noeud.couleur != 1 || element.noeud.couleur != 2){
			explorer(element.fils, element.noeud);
			element.noeud.couleur = 2
			fmt.Println("Coloration node : ", element.noeud.id, " couleur : noir")
		}
	}
}

func main() {
	nodeA := node{id: "A", couleur: 0 }
	nodeB := node{id: "B", couleur: 0 }
	nodeC := node{id: "C", couleur: 0 }
	nodeD := node{id: "D", couleur: 0 }
	nodeE := node{id: "E", couleur: 0 }
	nodeF := node{id: "F", couleur: 0 }
	nodeG := node{id: "G", couleur: 0 }

	graphFilsC_G := graph{
		fils: nil,
		noeud: nodeG,
	}

	graphFilsA_C := graph{
		fils : []graph{graphFilsC_G},
		noeud: nodeC,
	}

	graphFilsA_E := graph{
		fils: nil,
		noeud: nodeE,
	}

	graphFilsB_D := graph{
		fils: nil,
		noeud: nodeD,
	}

	graphFilsB_F := graph{
		fils: nil,
		noeud: nodeF,
	}

	graphFilsA_B := graph{
		fils: []graph{graphFilsB_D, graphFilsB_F},
		noeud: nodeB,
	}

	graphMaster := graph{
		fils: []graph{graphFilsA_C, graphFilsA_E, graphFilsA_B},
		noeud: nodeA,
	}

	explorer(graphMaster.fils, graphMaster.noeud)

}
