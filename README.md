# Informatique répartie
Metayer Justin

## TP – algorithmique distribuée
### Partie 1 :
__Exercice n°1:__ Un sommet peut fermer la connexion sortante lorsque tous les sommets ont défini le leader. Ainsi le noeud leader peut fermer le canal.

__Exercice n°2:__ File _exo2.go_

__Exercice n°3:__ 

- Pour n sommet -> Voir le fichier *exo3_part1.go*
    - On ajoute juste de nouveaux channels et de nouvelles nodes
- Pour n sommet reglable avec function -> Voir *exo3_part1_v2*
- Pour n sommet sans suivre une suite logique (1,2,3,..) -> Voir fichier *exo3_part2.go*
    - On inverse la position des nodes 2 et 4
    
__Exercice n°4:__ File _exo4.go_

Pour reduire le nombre de messages, on peut envoyer un seul message au commencement.