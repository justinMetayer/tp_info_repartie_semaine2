package main

import (
	"fmt"
	"go/ast"
	"sync"
	"time"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

type Message struct {
	id int
	isLeader bool
}

type MessageSync struct {
	sync bool
}

type RingNode struct {
	inSyncChannel <-chan MessageSync
	inChannel <-chan Message
	outChannel chan<- Message
	localId int
	leaderId int
}

func NewRingNode(id int, inChannel <-chan Message, outChannel chan<- Message, inSyncChannel <-chan MessageSync) *RingNode {
	return &RingNode{inSyncChannel,inChannel, outChannel, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(wg *sync.WaitGroup) {
	// Done will be executed once the for loop is over, i.e., the inChannel is closed
	defer wg.Done()

	// Inialisation: every node sends its id to the next node on the ring
	fmt.Println("From", r.localId, ": Sending ", r.localId)
	r.outChannel <- Message{r.localId, false}

	alreadySend := false

	for receivedMessage := range r.inChannel {
		fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)

		//Si il n'y a pas de leader
		if(receivedMessage.isLeader == false){
			if(receivedMessage.id == r.localId){
				r.leaderId = r.localId
				for range r.inSyncChannel{
					r.outChannel<-Message{r.localId, true}
					fmt.Println("From", r.localId, ": Sending", r.localId, true)
					break
				}
			}else{
				if(receivedMessage.id > r.localId){
					for range r.inSyncChannel {
						r.outChannel <- Message{receivedMessage.id, false}
						fmt.Println("From", r.localId, ": Sending", receivedMessage.id, false)
						break
					}
				}
			}
		}else{
			r.leaderId = receivedMessage.id

			if(receivedMessage.id == r.localId){
				fmt.Println("\n[",r.localId,"] Je suis le leader")
				close(r.outChannel)
			}else{
				fmt.Println("\n[",r.localId,"] Le leader est la node :", receivedMessage.id)
				if(alreadySend == false){
					for range r.inSyncChannel {
						r.outChannel <- Message{receivedMessage.id, true}
						fmt.Println("From", r.localId, ": Sending", receivedMessage.id, true)
						break
					}
				}
				alreadySend = true
			}
		}
	}
	if(r.leaderId != r.localId){
		close(r.outChannel)
	}
}

func masterNodeSync(inSync chan MessageSync){
	for true {
		time.Sleep(5*time.Second)
		fmt.Println("-------------------------------------")
		inSync <- MessageSync{true}
		inSync <- MessageSync{true}
		inSync <- MessageSync{true}
	}
}

func main() {
	out1 := make(chan Message, 1)
	out2 := make(chan Message, 1)
	out3 := make(chan Message, 1)

	inSync := make(chan MessageSync, 1)

	node1 := NewRingNode(1, out3, out1, inSync)
	node2 := NewRingNode(2, out1, out2, inSync)
	node3 := NewRingNode(3, out2, out3, inSync)

	var wg sync.WaitGroup
	wg.Add(3)

	go masterNodeSync(inSync)

	go node1.Run(&wg)
	go node2.Run(&wg)
	go node3.Run(&wg)

	wg.Wait()
}
