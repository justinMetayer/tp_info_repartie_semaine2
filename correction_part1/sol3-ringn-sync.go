package main

import (
    "fmt"
    "time"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
    if x < y {
        return y
    }
    return x
}

type Message struct {
    id int
    isLeader bool
}

type RingNode struct {
    inChannel <-chan Message
    outChannel chan<- Message
    localId int
    maxId int
    leaderId int
}

func NewRingNode(id int, inChannel <-chan Message, outChannel chan<- Message) *RingNode {
    return &RingNode{inChannel, outChannel, id, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(sync chan int) {
    // Inialisation: every node sends its id to the next node on the ring
    fmt.Println("From", r.localId, ": Sending ", r.localId)
    g++
    r.outChannel <- Message{r.localId, false}

    for {
        // Wait sync from main
        <- sync

        select {
        // Receive message from neighbor
        case receivedMessage := <- r.inChannel:
            fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)

            switch {
            // Leader already known => do nothing
            case r.leaderId != 0 :

            // Receiving a leader message => propagate and terminate
            case receivedMessage.isLeader == true:
                g++
                done++
                fmt.Println("From", r.localId, ":", receivedMessage.id, "is the leader - over")
                r.leaderId = receivedMessage.id
                r.outChannel <- receivedMessage

            // I receive my own id => I am the leader, I send a leader message
            case receivedMessage.id == r.localId:
                g++
                done++
                fmt.Println("From", r.localId, ": I am the leader")
                r.leaderId = r.localId
                r.outChannel <- Message{r.localId, true}

            // Receiving someone else non leader id => propagate the largest id
            // Optimisation, only send if the id is higher than the highest id sent so far
            default:
                if receivedMessage.id > r.maxId {
                    r.maxId = receivedMessage.id
                    g++
                    fmt.Println("From", r.localId, ": Sending ", receivedMessage.id)
                    r.outChannel <- Message{receivedMessage.id, false}
                }
            }
        // No message has been received, do nothing
        default:
        }
    }
}

const NODE_NB = 4

var g int = 0
var done = 0

func main() {
    syncs := make([]chan int, NODE_NB)
    for i := 0; i < NODE_NB; i++ {
        syncs[i] = make(chan int, 1)
    }

    links := make([]chan Message, NODE_NB)
    for i := 0; i < NODE_NB; i++ {
        links[i] = make(chan Message, 1)
    }
    nodes := make([]*RingNode, NODE_NB)
    for i := 0; i < NODE_NB; i++ {
        next := (i + 1) % NODE_NB
        nodes[i] = NewRingNode(i, links[i], links[next])
        go nodes[i].Run(syncs[i])
    }

    for {
        // This is not so nice
        if done == NODE_NB {
            return
        }
        time.Sleep(time.Second)
        fmt.Println()
        for i := 0; i < NODE_NB; i++ {
            syncs[i] <- 0
        }
    }
}
