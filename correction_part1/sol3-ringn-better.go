package main

import (
    "fmt"
    "sync"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
    if x < y {
        return y
    }
    return x
}

type Message struct {
    id int
    isLeader bool
}

type RingNode struct {
    inChannel <-chan Message
    outChannel chan<- Message
    localId int
    maxId int
    leaderId int
}

func NewRingNode(id int, inChannel <-chan Message, outChannel chan<- Message) *RingNode {
    return &RingNode{inChannel, outChannel, id, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(wg *sync.WaitGroup) {
    defer wg.Done()

    // Inialisation: every node sends its id to the next node on the ring
    fmt.Println("From", r.localId, ": Sending ", r.localId)
    g++
    r.outChannel <- Message{r.localId, false}

    for receivedMessage := range r.inChannel {
        fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)

        switch {
            // Leader already known => do nothing
            case r.leaderId != 0 :

            // Receiving a leader message => propagate and terminate
            case receivedMessage.isLeader == true:
                g++
                fmt.Println("From", r.localId, ":", receivedMessage.id, "is the leader - over")
                r.leaderId = receivedMessage.id
                r.outChannel <- receivedMessage
                close(r.outChannel)

            // I receive my own id => I am the leader, I send a leader message
            case receivedMessage.id == r.localId:
                g++
                fmt.Println("From", r.localId, ": I am the leader")
                r.leaderId = r.localId
                r.outChannel <- Message{r.localId, true}
                close(r.outChannel)

            // Receiving someone else non leader id => propagate the largest id
            // Optimisation, only send if the id is higher than the highest id sent so far
            default:
                if receivedMessage.id > r.maxId {
                    r.maxId = receivedMessage.id
                    g++
                    fmt.Println("From", r.localId, ": Sending ", receivedMessage.id)
                    r.outChannel <- Message{receivedMessage.id, false}
                }
        }
    }
}

const NODE_NB = 4

var g int = 0

func main() {
    var wg sync.WaitGroup

    links := make([]chan Message, NODE_NB)
    for i := 0; i < NODE_NB; i++ {
        links[i] = make(chan Message, 1)
    }
    nodes := make([]*RingNode, NODE_NB)
    for i := 0; i < NODE_NB; i++ {
        next := (i + 1) % NODE_NB
        nodes[i] = NewRingNode(i, links[i], links[next])
        wg.Add(1)
        go nodes[i].Run(&wg)
    }

    wg.Wait()
    fmt.Println(g, "messages")
}
