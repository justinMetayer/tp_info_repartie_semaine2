package main

import (
    "fmt"
    "sync"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
    if x < y {
        return y
    }
    return x
}

type Message struct {
    id int
    isLeader bool
}

type RingNode struct {
    inChannel <-chan Message
    outChannel chan<- Message
    localId int
    leaderId int
}

func NewRingNode(id int, inChannel <-chan Message, outChannel chan<- Message) *RingNode {
    return &RingNode{inChannel, outChannel, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(wg *sync.WaitGroup) {
    defer wg.Done()

    // Inialisation: every node sends its id to the next node on the ring
    fmt.Println("From", r.localId, ": Sending ", r.localId)
    r.outChannel <- Message{r.localId, false}

    for receivedMessage := range r.inChannel {
        fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)

        switch {
            // Leader already known => do nothing
            case r.leaderId != 0 :

            // Receiving a leader message => propagate and terminate
            case receivedMessage.isLeader == true:
                fmt.Println("From", r.localId, ":", receivedMessage.id, "is the leader - over")
                r.leaderId = receivedMessage.id
                r.outChannel <- receivedMessage
                close(r.outChannel)

            // I receive my own id => I am the leader, I send a leader message
            case receivedMessage.id == r.localId:
                fmt.Println("From", r.localId, ": I am the leader")
                r.leaderId = r.localId
                r.outChannel <- Message{r.localId, true}
                close(r.outChannel)

            // Receiving someone else non leader id => propagate the largest id
            default:
                fmt.Println("From", r.localId, ": Sending ", Max(r.localId, receivedMessage.id))
                r.outChannel <- Message{Max(r.localId, receivedMessage.id), false}
        }
    }
}

func main() {
    out1 := make(chan Message, 1)
    out2 := make(chan Message, 1)
    out3 := make(chan Message, 1)

    node1 := NewRingNode(1, out3, out1)
    node2 := NewRingNode(2, out1, out2)
    node3 := NewRingNode(3, out2, out3)

    var wg sync.WaitGroup
    wg.Add(3)

    go node1.Run(&wg)
    go node2.Run(&wg)
    go node3.Run(&wg)

    wg.Wait()
}
