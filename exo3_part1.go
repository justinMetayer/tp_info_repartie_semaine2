package main

import (
    "fmt"
    "sync"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
    if x < y {
        return y
    }
    return x
}

type Message struct {
    id int
    isLeader bool
}

type RingNode struct {
    inChannel <-chan Message
    outChannel chan<- Message
    localId int
    leaderId int
}

func NewRingNode(id int, inChannel <-chan Message, outChannel chan<- Message) *RingNode {
    return &RingNode{inChannel, outChannel, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(wg *sync.WaitGroup) {
    // Done will be executed once the for loop is over, i.e., the inChannel is closed
    defer wg.Done()

    // Inialisation: every node sends its id to the next node on the ring
    fmt.Println("From", r.localId, ": Sending ", r.localId)
    r.outChannel <- Message{r.localId, false}

    alreadySend := false

    for receivedMessage := range r.inChannel {
        fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)

        //Si il n'y a pas de leader
        if(receivedMessage.isLeader == false){
            if(receivedMessage.id == r.localId){
                r.leaderId = r.localId
                r.outChannel<-Message{r.localId, true}
                fmt.Println("From", r.localId, ": Sending", r.localId, true)
            }else{
                if(receivedMessage.id > r.localId){
                    r.outChannel<-Message{receivedMessage.id, false}
                    fmt.Println("From", r.localId, ": Sending", receivedMessage.id, false)
                }else{
                    r.outChannel<-Message{r.localId, false}
                    fmt.Println("From", r.localId, ": Sending", r.localId, false)
                }
             }
        }else{
             r.leaderId = receivedMessage.id

             if(receivedMessage.id == r.localId){
                fmt.Println("\n[",r.localId,"] Je suis le leader")
                close(r.outChannel)
             }else{
                fmt.Println("\n[",r.localId,"] Le leader est la node :", receivedMessage.id)
                if(alreadySend == false){
                    r.outChannel <- Message{receivedMessage.id, true}
                    fmt.Println("From", r.localId, ": Sending", receivedMessage.id, true)
                }
                alreadySend = true
             }
        }
    }
    if(r.leaderId != r.localId){
        close(r.outChannel)
    }
}

func main() {
    out1 := make(chan Message, 1)
    out2 := make(chan Message, 1)
    out3 := make(chan Message, 1)
    out4 := make(chan Message, 1)
    out5 := make(chan Message, 1)

    node1 := NewRingNode(1, out5, out1)
    node2 := NewRingNode(2, out1, out2)
    node3 := NewRingNode(3, out2, out3)
    node4 := NewRingNode(4, out3, out4)
    node5 := NewRingNode(5, out4, out5)

    var wg sync.WaitGroup
    wg.Add(3)

    go node1.Run(&wg)
    go node2.Run(&wg)
    go node3.Run(&wg)
    go node4.Run(&wg)
    go node5.Run(&wg)

    wg.Wait()
}
