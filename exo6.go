package main

import (
	"fmt"
	"time"
)

const NODE_NB = 5

var g int = 0
var done = 0
var NBTOUR = 0

type Message struct {
	id       int
	isLeader bool
}
type RingNode struct {
	inChannel  []<-chan Message
	outChannel []chan<- Message
	localId    int
	maxId      int
	leaderId   int
}

func NewRingNode(id int, inChannel []<-chan Message, outChannel []chan<- Message) *RingNode {
	return &RingNode{inChannel, outChannel, id, id, 0}
}

// Main code for all nodes
func (r *RingNode) Run(sync chan int) {

	// Inialisation: every node sends its id to the next node on the ring
	fmt.Println("From", r.localId, ": Sending ", r.localId)
	g++
	for _, chanel := range r.outChannel {
		chanel <- Message{r.localId, false}
	}
	for {
		// Wait sync from main
		<-sync
		for _, chanel := range r.inChannel {
			fmt.Println(" [",r.localId,"] Nombre de tour : ", NBTOUR)
			select {

			// Receive message from neighbor
			case receivedMessage := <-chanel:
				fmt.Println("From", r.localId, ": received", receivedMessage.id, receivedMessage.isLeader)
				switch {
				// Leader already known => do nothing
				case r.leaderId != 0:

				// Receiving a leader message => propagate and terminate
				case receivedMessage.isLeader == true:
					g++
					done++
					fmt.Println("From", r.localId, ":", receivedMessage.id, "is the leader - over")
					r.leaderId = receivedMessage.id
					for _, channel := range r.outChannel {
						channel <- receivedMessage
					}

				// I receive my own id => I am the leader, I send a leader message
				case NBTOUR >= 4 && receivedMessage.id == r.localId:
					g++
					done++
					fmt.Println("From", r.localId, ": I am the leader")
					r.leaderId = r.localId
					for _, channel := range r.outChannel {
						channel <- Message{r.localId, true}
					}
				// Receiving someone else non leader id => propagate the largest id
				// Optimisation, only send if the id is higher than the highest id sent so far
				default:
					if receivedMessage.id > r.maxId {

						r.maxId = receivedMessage.id
						g++
						fmt.Println("From", r.localId, ": Sending ", receivedMessage.id)
						for _, channel := range r.outChannel {
							channel <- Message{receivedMessage.id, false}
						}
					}
				}

			// No message has been received, do nothing
			default:
			}
		}
	}
}

func main() {
	nbChan := 7
	syncs := make([]chan int, NODE_NB)
	for i := 0; i < NODE_NB; i++ {
		syncs[i] = make(chan int, 1)
	}

	/*links := make([]chan Message, NODE_NB)
	for i := 0; i < NODE_NB; i++ {
		links[i] = make(chan Message, 1)
	}
	nodes := make([]*RingNode, NODE_NB)
	for i := 0; i < NODE_NB; i++ {
		next := (i + 1) % NODE_NB
		nodes[i] = NewRingNode(i, links[i], links[next])
		go nodes[i].Run(syncs[i])
	}*/

	tabChan := make([]chan Message, nbChan)
	for i := 0; i < nbChan; i++ {
		tabChan[i] = make(chan Message, 1)
	}

	chan1out := make([]chan<- Message, 2)
	chan1out[0] = tabChan[0]
	chan1out[1] = tabChan[1]
	chan1in := make([]<-chan Message, 1)
	chan1in[0] = tabChan[6]

	chan2out := make([]chan<- Message, 2)
	chan2out[0] = tabChan[3]
	chan2out[1] = tabChan[5]
	chan2in := make([]<-chan Message, 1)
	chan2in[0] = tabChan[0]

	chan3in := make([]<-chan Message, 1)
	chan3in[0] = tabChan[1]
	chan3out := make([]chan<- Message, 1)
	chan3out[0] = tabChan[2]

	chan4in := make([]<-chan Message, 3)
	chan4in[0] = tabChan[2]
	chan4in[1] = tabChan[5]
	chan4in[2] = tabChan[4]
	chan4out := make([]chan<- Message, 1)
	chan4out[0] = tabChan[6]

	chan5in := make([]<-chan Message, 1)
	chan5in[0] = tabChan[3]
	chan5out := make([]chan<- Message, 1)
	chan5out[0] = tabChan[4]

	node1 := NewRingNode(1, chan1in, chan1out)
	node2 := NewRingNode(2, chan2in, chan2out)
	node3 := NewRingNode(3, chan3in, chan3out)
	node4 := NewRingNode(4, chan4in, chan4out)
	node5 := NewRingNode(5, chan5in, chan5out)

	var tabNode []*RingNode
	tabNode = append(tabNode, node1)
	tabNode = append(tabNode, node2)
	tabNode = append(tabNode, node3)
	tabNode = append(tabNode, node4)
	tabNode = append(tabNode, node5)

	for i := 0; i < NODE_NB; i++ {
		go tabNode[i].Run(syncs[i])
	}

	for {
		// This is not so nice
		if done == NODE_NB {
			return
		}
		time.Sleep(time.Second)
		fmt.Println()
		NBTOUR++
		for i := 0; i < NODE_NB; i++ {

			syncs[i] <- 0
		}
	}
}
