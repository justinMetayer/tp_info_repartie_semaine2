package main

import (
	"encoding/json"
	"fmt"
	 "sync"
	"io/ioutil"
	"strconv"
	"time"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

type Message struct {
	id int
	//   isLeader bool
}

type GraphNode struct {
	inChannels  []<-chan Message
	outChannels []chan<- Message
	localId     int
	maxId       int
	leaderId    int
}

type GraphData struct {
	Graph Graph
}

type Graph struct {
	Nodes []Node
	Edges []Edge
}

type Node struct {
	Id string
}

type Edge struct {
	Source string
	Target string
}

func NewGraphNode(id int) *GraphNode {
	return &GraphNode{nil, nil, id, id, 0}
}

// Main code for all nodes
func (r *GraphNode) Run(sync chan int, wg *sync.WaitGroup) {
	// Inialisation: every node sends its id to the next node on the ring
	 defer wg.Done()
	recievedMsg := make([]Message, len(r.inChannels))
	for timer := range sync{
		// Wait sync from main
		

		fmt.Println("From", r.localId, ": Sending ", r.maxId)
		
		for i := 0; i < len(r.outChannels); i++ {
			r.outChannels[i] <- Message{r.maxId}
			g++
		}

		for i := 0; i < len(r.inChannels); i++ {

			// Receive message from neighbor
			recievedMsg[i] = <-r.inChannels[i]
			fmt.Println("From ", r.localId, " : received ", recievedMsg[i].id)
			r.maxId = Max(r.maxId, recievedMsg[i].id)
		}

		if timer == diam {
			if r.localId!=r.maxId{
			fmt.Println("From ", r.localId, " : ", r.maxId, " is the leader")
			}else{
			fmt.Println("From ", r.localId, " : I am the leader")
			}
			r.leaderId = r.maxId

		}

	}
}

var g int = 0

var diam = 4

func main() {

	//filename := "graph1.json"
	filename := "correction_part2/graph1.json"

	file, _ := ioutil.ReadFile(filename)
	data := GraphData{}
	err := json.Unmarshal(file, &data)

	if err != nil {
		fmt.Println("error:", err)
	}
	nb_nodes := len(data.Graph.Nodes)
	nb_edges := len(data.Graph.Edges)

	var wg sync.WaitGroup

	syncs := make([]chan int, nb_nodes)
	for i := 0; i < nb_nodes; i++ {
		syncs[i] = make(chan int, 1)
	}

	links := make([]chan Message, nb_edges)
	for i := 0; i < nb_edges; i++ {
		links[i] = make(chan Message, 1)
	}

	nodes := make([]*GraphNode, nb_nodes)
	for i := 0; i < nb_nodes; i++ {
		nodes[i] = NewGraphNode(i)
		// wg.Add(1)
		// go nodes[i].Run(&wg)
	}

	for i := 0; i < nb_edges; i++ {
		s, _ := strconv.Atoi(data.Graph.Edges[i].Source)
		t, _ := strconv.Atoi(data.Graph.Edges[i].Target)
		nodes[s-1].outChannels = append(nodes[s-1].outChannels, links[i])

		nodes[t-1].inChannels = append(nodes[t-1].inChannels, links[i])
	}

	for i := 0; i < nb_nodes; i++ {

		fmt.Println("Node ", i+1, " Inchanels : ", len(nodes[i].inChannels))
		fmt.Println("Node ", i+1, " Outchanels : ", len(nodes[i].outChannels))

	}

	for i := 0; i < nb_nodes; i++ {

		  wg.Add(1)
		go nodes[i].Run(syncs[i],&wg)
	}

	for i := 1; i <= diam+1; i++ {
		// This is not so nice

		time.Sleep(time.Second)
		if i < diam+1{
		fmt.Println("Round ", i)
		}
		for j := 0; j < nb_nodes; j++ {
			if i < diam+1{
			syncs[j] <- i
			} else{
			close(syncs[j])
			}
		}
	}
	wg.Wait()
	fmt.Println(g, "  messages en tout")
	

}
