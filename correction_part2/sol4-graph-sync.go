package main

import (
    "fmt"
    "time"
)

// Max returns the larger of x or y.
func Max(x, y int) int {
    if x < y {
        return y
    }
    return x
}

type Message struct {
    id int
    isLeader bool
}

type Node struct {
    in_neighbors []chan Message
    out_neighbors []chan Message
    localId int
    maxId int
    leaderId int
}

func NewNode(id int, in_neighbors []chan Message, out_neighbors []chan Message) *Node {
    return &Node{in_neighbors, out_neighbors, id, id, 0}
}

// Main code for all nodes
func (r *Node) Run(sync chan int, diameter int) {

    // Inialisation: every node sends its id to all neighbors
    for _, e := range r.out_neighbors {
        fmt.Println("From", r.localId, ": Sending", r.localId, "to", e)
        e <- Message{r.localId, false}
    }

    for i := 0; i < diameter; i++ {
        // Wait sync from main
        <- sync

        // receive all messages from neighbors and keep the maximal value received
        for _, e := range r.in_neighbors {
            select {
            // Receive message from neighbor
            case receivedMessage := <- e:
                if receivedMessage.id > r.maxId {
                    r.maxId = receivedMessage.id
                }
                fmt.Println("From", r.localId, ": received", receivedMessage.id, "from", e)
            }
        }

        // send all messages to neighbors with the maximal value
        for _, e := range r.out_neighbors {
            fmt.Println("From", r.localId, ": Sending", r.maxId, "to", e)
            e <- Message{r.maxId, false}
        }    
    }

    fmt.Println("From", r.localId, ": leader is", r.maxId)
    return
}

var g int = 0
var done = 0

func main() {
    // Synchronisation channels
    syncs := make([]chan int, 5)
    for i := 0; i < 5; i++ {
        syncs[i] = make(chan int, 1)
    }

    // Graph channels (one in each direction)
    var link12 = make(chan Message, 1)
    var link23 = make(chan Message, 1)
    var link24 = make(chan Message, 1)
    var link34 = make(chan Message, 1)
    var link35 = make(chan Message, 1)
    var link21 = make(chan Message, 1)
    var link32 = make(chan Message, 1)
    var link42 = make(chan Message, 1)
    var link43 = make(chan Message, 1)
    var link53 = make(chan Message, 1)

    var in_neighs1 = []chan Message {link21}
    var in_neighs2 = []chan Message {link12, link32, link42}
    var in_neighs3 = []chan Message {link23, link43, link53}
    var in_neighs4 = []chan Message {link24, link34}
    var in_neighs5 = []chan Message {link35}
    var out_neighs1 = []chan Message {link12}
    var out_neighs2 = []chan Message {link21, link23, link24}
    var out_neighs3 = []chan Message {link32, link34, link35}
    var out_neighs4 = []chan Message {link42, link43}
    var out_neighs5 = []chan Message {link53}

    nodes := make([]*Node, 5)
    nodes[0] = NewNode(1, in_neighs1, out_neighs1)
    nodes[1] = NewNode(2, in_neighs2, out_neighs2)
    nodes[2] = NewNode(3, in_neighs3, out_neighs3)
    nodes[3] = NewNode(4, in_neighs4, out_neighs4)
    nodes[4] = NewNode(5, in_neighs5, out_neighs5)

    // diameter of the graph, only that number of synchronisations is made
    diameter := 3

    for i := 0; i < 5; i++ {
        go nodes[i].Run(syncs[i], diameter)
    }

    for i := 0; i < diameter + 1; i++ {
        time.Sleep(time.Second)
        fmt.Println()
        fmt.Println("Round", i + 1)
        for i := 0; i < 5; i++ {
            syncs[i] <- 0
        }
    }
}
